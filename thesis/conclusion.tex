\chapter{Conclusion}\label{chap:conclusion}

\section{Summary}\label{sect:conclusion-summary}

One often hears about how fast things change, especially in the realm of
computer science. While being first specified in the ``ancient'' times of 1970s
\citerfcp{rfc675}, \gls{TCP} is still the first choice for most general
applications on the Internet. \erratumReplace[type={grammar}]{It's}{Its} ease of
use and reliability guarantees makes
it a solid choice of protocol for applications that do not transmit
time-dependent data. While there are alternatives to \gls{TCP}, it would be of
great benefit to improve \gls{TCP}'s performance for applications with strict
latency requirements.

The \gls{RDB} mechanism we have developed and tested, does not aim to replace
the default \glspl{CC} used today, but to be an easy alternative for
applications that have strict latency requirements, and for various reasons need
to use \gls{TCP}.

In the later years, research has started focusing on the latency performance of
\gls{TCP}, with new mechanisms emerging, like \gls{LT}, \gls{mFR}, \gls{ER} and
\gls{TLP}. What all these mechanisms have in common is that they try to minimize
the time before lost packets are retransmitted by the \gls{tcp-engine}. The
problem is that they wait until they believe it is likely that a loss has
occurred. This means that the per-packet latencies for lost packets are increased
by a minimum of one \gls{RTT}, but often it can be more.
\erratumNote[type={readability}]{Merged paragraph}
%
This is where \gls{RDB} takes a different path, and retransmits un-\glsed{ACK}
data segments simply because it \emph{may} be(come) lost. By bundling old
un-\glsed{ACK} data that probably will be redundant, it will proactively ensure
that the per-packet (or per-segment) latency is not drastically reduced in case
of loss.
%
We argue that this can be justified because \gls{RDB} does not create extra
packets on the wire by ensuring to create packets that are below the \gls{MSS}
limit.

The \gls{TFRC} \gls{CC} we have implemented in \gls{rdb2} raises the \gls{CWND}
for \glspl{thin-stream} compared to the latest version of the
\linuxkernel[3.19]{}, which causes more packets on the wire in some cases. This
is not because it allows the stream to send more data, but because, the data is,
to a greater degree, queued on the sender side, when the \gls{CWND} is a
limiting factor.

Through experiments we have confirmed the vast improvements in latencies
reported by \cite{petlund-2009} when using \gls{RDB}. We have also verified that
the \gls{rdb2} implementation is \gls{TCP} fair, at least within the
scope of our test environment.


\section{Contributions}\label{sect:conclusion-contributions}

The work in this thesis has focused on how to improve the latency for
\glspl{thin-stream} using \gls{TCP}. While some recent work has focused on
mechanisms for reducing the retransmission delay for \glspl{thin-stream} over
\gls{TCP} (\cref{sect:thin-stream-modifications}), the lost data segments still
require retransmissions. The \gls{RDB} mechanism modifies the packets sent by
\gls{TCP} by bundling already sent data with the packets containing new,
un-transmitted data. By bundling redundant data with the data packets,
\erratumReplace[type={correction}, description={Be more precise}]{the}{an
  RDB-stream} can recover from sporadic packet loss without having to wait for a
retransmission from the sender host.

Based on earlier work on \gls{RDB} \citep{evensen-2008,petlund-2009}, this
thesis tries to tame the \gls{RDB} mechanism, to ensure that the latency
improvements for the streams utilizing \gls{RDB} is balanced against the negative
effects inflicted on competing traffic.

\vspace{8mm}
\noindent The main contributions of this thesis can be summarized as follows:

\begin{enumerate}[leftmargin=1cm,itemindent=0mm,label=\large\textbullet,parsep=0pt]

\item{ We have evaluated the \glsuseri{thin-stream} classification mechanism in
    the \linuxkernel{}. Further, we have developed an alternative method, which
    uses the \gls{RTT}, together with the user space application's transmission
    patterns, to identify if the stream is thin.}

\item{ We have identified an issue with recent changes to the \linuxkernel{}'s
    \gls{tcp-engine} that severely affects the latency for \glspl{thin-stream}
    negatively.}

\item{Implementation of \gls{RDB} as a \linuxkernel{} module, that enables a
    sender host to transmit data to a \gls{TCP} receiver using the \gls{RDB}
    technique.}

\item{To help \glspl{thin-stream} maintain a more stable \gls{throughput} rate
    than provided by standard \gls{TCP}, we have implemented a \gls{CC} in the
    kernel module which utilizes the equation based throughput calculation for
    \gls{TFRC-SP}.}

  \item{Investigated and developed methods for detecting packet loss that \gls{RDB}
    hides from the \gls{tcp-engine}.}

\end{enumerate}

\vspace{2mm}
\noindent The main contributions from the experiments performed on \gls{RDB}, can be
summarized as follows:

\begin{enumerate}[leftmargin=1cm,itemindent=0mm,label=\large\textbullet,parsep=0pt]
\item{Investigating how the redundant bundling mechanism performs under different
    test scenarios, and finding a tradeoff between improving the latency and
    increasing the payload size when bundling redundant data.}

\item{The latency gains achieved with different bundling rates, i.e.,
    limitations to how much data can be bundled with each packet.}

\item{How \gls{RDB} streams affect competing traffic under different test
    scenarios.}

\item{How \gls{RDB} can be prevented from being abused to obtain an advantage
    over competing traffic.}

\end{enumerate}


\section{Future work}\label{sect:conclusion-future-work}

\Glspl{thin-stream} over \gls{TCP} have been overlooked for a long time, and
require further attention to be able to compete with other transport protocols.
Some candidates for future work is outlined next: \erratumDelete[type={typo},
description={Removed rogue sentense}]{Work specifically on \gls{RDB}.}


\begin{enumerate}[leftmargin=1cm,itemindent=0mm,label=\large\textbullet,parsep=0pt]
  \litem{Investigate the recent changes to the Linux kernel}
  Recent changes to the Linux kernel's \gls{tcp-engine} seem to affect the
  latency of \glspl{thin-stream} (described in
  \cref{Recent-changes-to-the-Linux-kernel-TCP}\erratumAdd[description={missing
    paranthesis}]{)}. The negative effects these
  changes have, may force current applications sending \glspl{thin-stream} over
  \gls{TCP} over on other protocols. This issue should be investigated further,
  firstly by finding out if the behavior is intended or not. Hopefully it can be
  regarded as a bug, so that work on how to fix and improve the situation can be
  done. If it is intended, the standards need to be changed if
  \glspl{thin-stream} should have a future together with \gls{TCP}.

  \litem{Improving the loss detection mechanisms for \gls{RDB}.} Of the
  techniques we presented in \cref{sect:loss-detection} for detecting loss, only
  the technique looking at multiple \glsed{ACK} packets
  \erratumReplace[type={grammar}]{were}{was} implemented in
  \gls{rdb2}. As this technique is reliable as long as there is no packet
  reordering or loss of \gls{ACK} packets, it may perform worse in the ``real
  world''. However, the lack of mechanisms for detecting reordering can only
  lead to an over-estimation of the loss rate, which does not lead to unfairness
  in favor of the \gls{RDB} streams.

  Further improvements to the loss detection can be made by implementing the
  technique detailed in \cref{implementation:DSACK}, where the \gls{DSACK}
  information in the \glspl{ACK} is utilized to identify which packet was lost.

  \litem{Performance profiling}
  To measure the resource overhead caused by the different parts of the module
  it is necessary to perform profiling.

  \litem{Measure the true \Gls{application-layer-latency}}
  Due to the limitations of the \gls{ACK-latency} measurement explained in
  \cref{sect:evaluation-metrics-application-layer-latency}, further work
  should be done on finding a solution on how to measure the
  \gls{application-layer-latency}. This is important to get more precise
  latency measurements, allowing a fair comparison between the results of
  \erratumReplace[type={grammar}, description={Fix grammar}]{mechanisms and
    takes}{different mechanisms, by taking} into account the sender side
  \gls{queuing-delay}.

  \begin{enumerate}[itemindent=0mm,label=\large\textbullet,parsep=0pt]
  \item{One possible solution is to use kprobes to hook into the relevant functions in
      the kernel to get the timestamps of when the
      \erratumDelete[type={grammar}, description={Duplicate word}]{the} payload
      enters and leaves the kernel.}
  \item{Another solution could be to implement this in the sender application such as
      streamzero, that saves the timestamps for the payload. By using the same computer
      for sending and receiving, the clock drift issue can be evaded.}
  \end{enumerate}
\end{enumerate}
