\section{RDB prototype v1}\label{chap:chapter3}

\gls{RDB} started out as an idea in \cite{paaby-2006}, to extend the
\code{retrans_collapse} function in the \linuxkernel{}. The function
\code{tcp_retrans_try_collapse} in \code{tcp_output.c} is used to collapse
(merge) \glspl{SKB} in the \gls{tcp-output-queue} before retransmition. It will
try to merge as many \glspl{SKB} as possible. In the \linuxkernel[2.6.15], which
they were studying, the function \code{tcp_retrans_try_collapse} was only called
once to merge two adjacent \glspl{SKB}, so the idea was to modify this to do
this multiple times, as the current \linuxkernel{} code does.

Building upon this idea, \cite{evensen-2008} implemented a more aggressive
bundling mechanism that also bundles old data when transmitting packets with new
data. These modifications, which we will refer to as the \gls{rdb1},
modifies the buffers in the \gls{tcp-output-queue} by prefixing the payload of
previous buffers to each new buffer. \gls{rdb1} was implemented in the
\linuxkernel[2.6.22.1] and later ported to version 2.6.23.8
\citep[29]{evensen-2008}.

The \gls{rdb1} patch, which is included in
\cref{code-linux-kernel-thin-stream-patch}, was sent to the Linux net-dev
mailing lists in 2009 for feedback. No more work was done on the
\gls{rdb1} patch after that. The implementation details will be
breifly described in \cref{sect:prototype-implementation}.


\begin{figure}
\begin{figurebox}[label={figure:tcp-output-callgraph}]
\centering
\resizebox{\textwidth}{!}{\includestandalone{tcp-linux-callgraph} }
\end{figurebox}
\caption{Call graph of parts of the \gls{tcp-engine} in the \linuxkernel{}}
\end{figure}


\subsection{TCP-engine in Linux}\label{sect:tcp-in-linux}

The part of the \linuxkernel{} that implements \gls{TCP} is called the
\gls{tcp-engine}. The main parts of the \gls{tcp-engine} code in the
\linuxkernel{} is implemented in the following files:

\begin{enumerate}[leftmargin=1cm,itemindent=-5mm,label=\large\textbullet]
  \litem{\code{tcp.c}}
  Contains initialization code for a \gls{TCP} connection and the entry points
  for data comming from user space.
  \litem{\code{tcp_input.c}}
  Handles incomming packets and processes the events that are triggered by
  \glspl{ACK}.
  \litem{\code{tcp_output.c}}
  Processes outgoing packets contained in the \gls{tcp-output-queue} and sends
  the \glspl{SKB} to the IP-layer for transmission.
  \litem{\code{tcp_cong.c}}
  Contains the \gls{tcp-new-reno} \gls{CC} code as well as the base code for the
  \gls{linux-CC-framework}.
  \litem{\code{tcp_<CC>.c}}
  Contains alternative \gls{CC} implementations, (where $CC$ is replaced with)
  such as \gls{tcp-cubic}, \code{vegas}, \code{highspeed}, \code{westwood} and
  \code{veno}.
\end{enumerate}

\Cref{figure:tcp-output-callgraph} illustrates the parts of the \gls{tcp-engine}
implemented in \code{tcp.c}, where data enters from user space, and
\code{tcp_output.c}, where it is sent to the IP layer.

\begin{figure}%[H]
\begin{figurebox}[label={figure:tcp-output-queue}]
\centering
\resizebox{0.8\textwidth}{!}{\includestandalone{tcp-output-queue} }
\end{figurebox}
\caption[The TCP output queue]{The \gls{tcp-output-queue}}
\end{figure}


\subsubsection*{SKBs and the TCP output engine}\label{sect:skbs-and-tcp-output-engine}

The \gls{tcp-output-engine} stores the outgoing data for each socket in a linked
list called the \gls{tcp-output-queue} as shown in
\cref{figure:tcp-output-queue}. The data to be transferred is stored in
\glspl{SKB} which is a \code{struct} containing next and previous pointers as
well as the necessary information needed to build the network packets.

The \gls{tcp-output-queue} has two functions:
\begin{inlinelist}
  \initem{} to handle the send rate mismatch between the application layer and
  \gls{TCP}, and

  \initem{} to store the sent packets, in case of a needed retransmission, until
  they are acknowlegded.
\end{inlinelist}

The elements are ordered by the sequence numbers where \code{sk_write_queue}
points to the first list element that contains the oldest un-\glsed{ACK} data,
whereas \code{sk_send_head} points to the data that has not yet been sent. If
they point to the same buffer, no un-\glsed{ACK} data is present in the queue.
If only \code{sk_send_head} is \code{NULL}, all the data in the queue has been
sent and is waiting to be acknowledged.

When \glspl{ACK} are received, the function \code{tcp_clean_rtx_queue} is
called to traverse the \gls{tcp-output-engine} to remove \glspl{SKB} if
possible.


\subsection{RDB prototype 1 (RDBv1)}\label{sect:prototype-implementation}

As depicted in \cref{figure:rdb-protype-callgraph-changes}, data from the
application layer enters the \gls{tcp-engine} in \code{tcp_sendmsg}.
\code{tcp_sendmsg} is modified to call the function \code{tcp_trans_merge_prev}
after the new data has been inserted into the \gls{tcp-output-queue}.
\code{tcp_trans_merge_prev} does the work of modifying the new \glspl{SKB} by
placing older data into the beginning of the data area. For the incomming
packets, \code{tcp_clean_rtx_queue} is modified to clean up after the changes
made to the \gls{tcp-output-queue}, as shown in
\cref{figure:rdb-protype-incomming}.


\begin{figure}%[H]
\centering
\begin{figurebox}[label={figure:rdb-protype-callgraph-changes},
  boxsep=0mm,
  left=0mm,
  right=0mm,
  top=0mm,
  bottom=0mm,
  arc=0mm,
  outer arc=0mm,
%  enlarge bottom finally by=5mm,
%  enlarge by=2mm,
%  show bounding box=red,
  ]
%\resizebox{\textwidth}{!}{%
\centering
\resizebox{\textwidth}{!}{\includestandalone{rdb-protype-callgraph-changes} }
%\includestandalone{rdb-protype-callgraph-changes}
\end{figurebox}

\caption[The call sequence for outgoing data in the \glstext{rdb1} prototype
implementation]%
{The call sequence for outgoing data in the \gls{rdb1} prototype implementation.
  Nodes with red text are functions that were modified. Nodes with brown border
  are new functions.}
\end{figure}



\begin{figure}%[H]
\centering
\begin{figurebox}[label={figure:rdb-protype-incomming},
  boxsep=0mm,
  left=0mm,
  right=0mm,
  top=0mm,
  bottom=0mm,
  arc=0mm,
  outer arc=0mm,
%  enlarge bottom finally by=5mm,
%  enlarge by=2mm,
%  show bounding box=red,
  ]
%\resizebox{\textwidth}{!}{%
\centering
\includestandalone{rdb-protype-incomming}
\end{figurebox}

\caption[The call sequence for incomming packets in the RDB prototype
implementation]%
{The call sequence for incomming packets in the \glstext{rdb1} prototype
  implementation. The node marked with red text is modified.}
\end{figure}

A prerequisite for the \gls{RDB} technique, is that the receiver end will check
the incomming packets by using the end sequence number, and not the start
sequence number. If only the start sequence number was to be compared with the
expected incomming sequence number, it would classify \gls{RDB} packets as
retransmitted packets and ignore them.

%In \cref{code-tcp-data-queue} we see the function \code{tcp_stream_is_thin},

The function in the \linuxkernel{}'s \gls{tcp-engine} that handles incomming
data packets, \code{tcp_stream_is_thin}, is shown in \cref{code-tcp-data-queue}.

%  \refhypname{\code{tcp_stream_is_thin}}{code-tcp-stream-is-thin}

In \cref{code-tcp-data-queue} \cref{has-new-data-lineref}, we see that it first
checks if the starting sequence number is in sequence by testing if it equals
\code{rcv_nxt}. If that is not the case it will check if the incomming \gls{SKB}
contains new data, by testing if \code{end_seq} is greater than the
\code{rcv_nxt} (\cref{has-new-data-lineref}). This ensures that the parts of the
data that is new in an \gls{RDB} packet is still processed even the when start
sequence number is not the expected sequence number for new data.

\begin{ccode}[%
  listing and comment, % Override default value (listing only)
  comment={The initial tests in \code{tcp_data_queue} on the incoming
    data.\newline%
    Four punctuations indicate code lines that were removed. Comments added for
    clarity are marked in {\color{codecommentcolor} this color}},%
  label={code-tcp-data-queue},%
  title={Linux kernel source},
%Excerpt from function tcp_data_queue in tcp_input.c
  ]
static void tcp_data_queue(struct sock *sk, struct sk_buff *skb)
{
    if (TCP_SKB_CB(skb)->seq == tp->rcv_nxt) {@\label{in-sequence-lineref}@
        // This is an in sequence segment
        ....
queue_and_out:
        ....
        return
    }
    if (!after(TCP_SKB_CB(skb)->end_seq, tp->rcv_nxt)) {@\label{has-new-data-lineref}@
        /* A retransmit, 2nd most common case. Force an immediate ack. */
        // Does not contain any new data
        ....
out_of_window:
        tcp_enter_quickack_mode(sk);@\label{in-sequence-quickack1-lineref}@
        inet_csk_schedule_ack(sk);
        return
    }
    /* Out of window. F.e. zero window probe. */
    if (!before(TCP_SKB_CB(skb)->seq, tp->rcv_nxt + tcp_receive_window(tp)))
        goto out_of_window:
    tcp_enter_quickack_mode(sk);@\label{in-sequence-quickack2-lineref}@

    if (before(TCP_SKB_CB(skb)->seq, tp->rcv_nxt)) {@\label{partial-data-lineref}@
        /* Partial packet, seq < rcv_next < end_seq */
        // This is where RDB packets containing both new and old data are handled
        ....
        tcp_dsack_set(sk, TCP_SKB_CB(skb)->seq, tp->rcv_nxt);@\label{partial-data-dsack-lineref}@
        ....
        if (!tcp_receive_window(tp))
            goto out_of_window;
        goto queue_and_out;
    }
}
\end{ccode}
\docaptionof{codelisting} {Excerpt from the function \code{tcp_data_queue} in
  \code{tcp_input.c}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Issues and critique of RDB}\label{sect:implementation-motivation}

Concerns have been raised as to the aggressiveness of \gls{RDB}, and how it
could potentially affect other competing network streams. The
\gls{rdb1} has a sysctl option (\code{tcp_rdb_max_bundle_bytes}) that
limits the number of bytes to bundle for each packet, but it does not use the
\code{tcp_stream_is_thin} employed in \gls{mFR} and \gls{LT} to bundle only when
a stream has less than 4 \glspl{PIF}.

Concerns were also raised about how the \glspl{SKB} in \gls{tcp-output-queue}
are modifed by adding old data to new \glspl{SKB}. These complex changes to the
\glspl{SKB}, which are performed when new data enters the kernel, as well as
before retransmitions, may lead to issues like silent data corruption, and would
require an audit for every place where changes to the data are made to be sure
the data is always correct (see
\cref{comment:ilpo-on-rdb-prototype-output-queue-mod}).
