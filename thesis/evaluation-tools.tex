\section{Tools}

We have used a wide range of tools to perform the experiments and analyse the
results. The tools we have developed in-house are explained briefly.

\subsection{sshscheduler}

\gls{sshscheduler} is a python script we developed to run the experiments in the
testbed. It it designed for easily generating a large number of jobs with
different test properties. Implementing strict return value verification, and
full logs for every command, makes it easy to identify errors and
misconfigurations in the tests.

\subsection{graph_r}

\gls{graph-r} is a collection of python scripts we have developed to analyse
data and generate plots based on the results of the testbed experiments. By
providing a directory containing pcap files from a test run, it will analyse the
file names and extract the test parameters. Depending on which types of results
is requested, it will run a set of commands to produce different types of
results, like \gls{ACK-latency}, loss statistics, \gls{throughput},
\gls{goodput}, and \gls{CWND}. Further, the results
\erratumReplace[type={grammar}, description={Fix typo}]{of}{are} plotted with
\gls{R} using the python wrapper \gls{rpy2}.

\subsection{streamzero}

We have used \gls{streamzero} to generate network streams, a program developed
specifically for the task of testing \glspl{thin-stream}. It can set up multiple
network streams that send data simultaneously, using either \gls{TCP} or
\gls{UDP}. It is similar to \name{iperf}, but has a few extra features
implemented for our use cases.
%
Important features that we have used when testing:

\begin{enumerate}[leftmargin=1cm,itemindent=0mm,label=\large\textbullet,parsep=0pt]
  \litem{Send streams with defined packet size/\gls{ITT} or bandwidth}
  When sending thin streams it does \code{send} calls to the kernel with small
  chunks of data to try to make the kernel send small packets, but it cannot
  guarantee small packets being sent, as the kernel may merge the data into
  bigger chunks depending on many factors and settings.

  \litem{Specify randomly varying \gls{ITT} and packet size with standard deviation.}
  This is important to avoid synchronous transmits when testing with multiple
  streams per host. By specifying an \gls{ITT} using the notation $X:Y$, the
  time between each data segment is sent to the kernel is randomly chosen from a
  normal distribution of mean $X$ and standard deviation $Y$. The random
  variation is computed using a Box–Muller transform of the pseudo-random
  normally distributed numbers produced by \code{drand48}, as shown in
  \cref{code-get-negexp}.

  \bitem{Specifying socket options}
  \begin{enumerate}[itemindent=0mm,label=\large\textbullet,parsep=0pt]
    \item{Enable \gls{tcp-thin-linear-timeouts} and \gls{tcp-thin-dupack}}
    \item{Disable \gls{tcp-nagle}}
    \item{Enable \gls{RDB}}
  \end{enumerate}

\end{enumerate}

\begin{command}[%
listing and comment, % Override default value (listing only)
listing file={include/commands/streamzero.out},%
label={command-streamzero},%
]
\end{command}
\docaptionof{commandlisting}{Running \name{streamzero}}

\begin{ccode}[%
%  listing and comment, % Override default value (listing only)
  label={code-get-negexp},%
  ]
int get_negexp_val(int mean, int stdev) {
    double rand = sqrt(-2 * log(drand48())) * cos(2 * M_PI * drand48());
    return rand * stdev + mean;
}
\end{ccode}
\docaptionof{codelisting}{Function in \name{streamzero} that generates pseudo-random numbers from a
  given mean and standard deviation.}

\subsubsection{Why variate the ITT?}\label{evaluation-why-variate-the-ITT}

We noticed some strange results in our tests, regarding the loss rates of the
individual streams from the hosts sending \glspl{thin-stream}. For some tests
the loss rates were varying to a great degree, where we expected them to be
fairly equal. We tested with up to 40 simultaneous \glspl{thin-stream}, and the
more streams, the greater the variation of loss. After investigating the issue
with many different tests, we found that the issue was caused by the
\glspl{thin-stream} sending data at regular intervals. When the total number of
streams exceed a threshold, that depends on the bottleneck queue size, a certain
number of streams will more often than others encounter a full queue, and hence
lose the packet. When the \gls{ITT} is not adjusted dynamically, the same
streams will end up continually sending at a bad spot right after other
\glspl{thin-stream} have sent their packets.

To illustrate this, the results in
\cref{thin-stream-mod-CDF-Streams:21-Streams-Greedy:10-Payload:120-ITT:100:15-RTT:150-PIFG:0-cubic-5-DA:0-LT:0-ER:3}
(from \cref{tests-of-thin-dupACK-and-Linear-Retransmission-Timeout-options})
have been plotted together with the results from rerunning the test using an
\gls{ITT} of \ms{100}, instead of \ms{100:10} as in the original tests.
%
In these plots
(\cref{thin-stream-mod-CDF-per-stream-Streams:21-Payload:120-RTT:150-cubic-5-ER:3})
the latency is plotted for each stream, and not aggregated. Comparing the loss
rates in the two the tables we see that the loss values of
\cref{thin-stream-mod-CDF-per-stream-Streams:21-Streams-Greedy:10-Payload:120-ITT:100-RTT:150-PIFG:0-cubic-5-DA:0-LT:0-ER:3-table}
has a greater variation than
\cref{thin-stream-mod-CDF-per-stream-Streams:21-Streams-Greedy:10-Payload:120-ITT:100:15-RTT:150-PIFG:0-cubic-5-DA:0-LT:0-ER:3-table}.

To avoid any synchronization issues with the \glspl{thin-stream} we therefore decided
to start the streams with a variable delay as well as using a random variation
for the \gls{ITT} of each \gls{thin-stream}.

\renewcommand{\subfigwidth}{0.40}
\renewcommand{\subfighorizontalspacing}{\hspace{2cm}}
\renewcommand\mainfigurevspace{}

\input{experiments/thin-stream-mod-CDF-per-stream-latex_figures_list}


\subsubsection{Contributions}

As part of this thesis, \gls{streamzero} has been in large parts rewritten and
extended with a set of features:

\begin{enumerate}[leftmargin=1cm,itemindent=0mm,label=\large\textbullet,parsep=0pt]

  \bitem{Stream intervals} which is a way to specify stream characteristics that
  last for a certain period. With $-I\:d:10,i:300,b:2 -I\;d:2,i:50,S:300:50$,
  two intervals are specified where the first interval has a 10 seconds
  duration, \gls{ITT} \erratumReplace[type={spelling}, description={Fix
    typo}]{or}{of} \ms{300}, and bandwidth of 2kbps, which means it will
  adjust the size of the payload argument to \code{send} to match this
  throughput. The second interval lasts for 2 seconds, has an \gls{ITT} of
  \ms{50} and an average \erratumReplace[type={refining}, description={Improve
    precision}]{packet}{payload} size of 300 bytes with a standard deviation of
  50. The specified intervals will be run in a round-robin fashion until the
  global duration expires.

  \bitem{Specifying a Congestion Control Algorithm} to use for the streams. This
  was a strict requirement after implementing \gls{rdb2}. The
  alternative is to set \gls{RDB} as the default \gls{CC}, but that would lead
  to all the \gls{TCP} connections using \gls{RDB} which easily causes trouble
  when there are bugs in the code.

  \bitem{Support for UDP traffic} instead of \gls{TCP}.

  \bitem{Bug fixes and performance improvements} allowing one sender host to run
  thousands of simultaneous threads.

  \litem{Verify the data integrity with SHA-1} Implementing SHA-1 data
  verification between the server and client helped to identify problems during
  the development of \gls{rdb2}.

\end{enumerate}


\subsection{analyseTCP}\label{sect:analysetcp}

\Gls{analysetcp} is a tool developed by the authors of \gls{mFR}, \gls{LT} and
\gls{rdb1} for analyzing \gls{tcpdump} trace (\name{pcap}) files. The
most important feature of \gls{analysetcp}\erratumReplace[type={grammar},
description={Remove unneeded words}]{that we have used}{,} that other tools do
not provide\erratumAdd[type={grammar}, description={Missing comma}]{,} is
analyzing both sender and receiver trace to calculate exact loss.
\gls{analysetcp} can also calculate the variation of one-way latency for the
packets by adjusting for clock drift on the two traces. It is also the only tool
that can handle \gls{RDB} packets, and differentiate between retransmitted and
\gls{RDB} data.
%\\[3mm]
%\noindent
\erratumReplace[type={grammar}, description={Improve
  readability}]{Important}{The following are the most important} features that
we have used when analyzing the results:

\begin{enumerate}[leftmargin=1cm,itemindent=-5mm,label=\large\textbullet]
  \litem{Calculating \gls{ACK-latency} based on ACKs}
  The \gls{ACK-latency}, described in \ref{sect:evaluation-metrics-ack-latency},
  is the main metric we have used to measure the latency differences for
  different \gls{TCP} variations.

  \litem{Loss estimation based on retransmissions}
  This is simply the ratio of \erratumAdd[type={grammar}, description={Missing
    word}]{the} number of \erratumReplace[type={grammar}, description={Should be
    plural}]{retransmission}{retransmissions} compared to
  \erratumAdd[type={grammar}, description={Missing word}]{the} number of packets
  sent. This gives a rough approximation of lost packets, which
  \erratumReplace[type={grammar}, description={Should be singular}]{are}{is}
  fairly correct in \erratumReplace[type={refining}, description={Improve
    intended meaning}]{some}{many} scenarios. In
  \erratumReplace[type={refining}, description={Improve
    readability}]{others}{some scenarios, however}, such as when using
  \gls{RDB}, the approximation is not useful as \gls{RDB} reduces the number of
  \erratumReplace[type={grammar}, description={Should be
    plural}]{retransmission}{retransmissions} drastically.

  \litem{Calculating exact packet and byte loss}
  This has been one of the most important features we have used for this thesis.
  By comparing the data from the sender side \name{pcap} file to the data in
  the receiver file, the exact amount of lost packets and bytes is calculated.
  This has been very useful to get a precise overview of the network environment
  the network streams are tested in.

\end{enumerate}


\subsubsection{Contributions}

As part of the work in this thesis, large parts of \gls{analysetcp}
\erratumReplace[type={grammar}, description={Fix typo}]{has}{have} been
rewritten. A range of bugs have been implemented and fixed in the process of
improving and stabilizing the code. At a certain point most traces were handled
properly, and most new issues we encountered were caused by corner cases in the
traces.
%
After comparing the results from \gls{analysetcp} with \gls{tcptrace} and
\gls{wireshark}/\name{tshark}, we are confident that it produces reliable
results.

\begin{enumerate}[leftmargin=1cm,itemindent=0mm,label=\large\textbullet,parsep=0pt]

  \bitem{Re-implementation of how the information for the data segments are
    stored}\erratumNote[type={readability}]{fixed incorrect newline}
  has enabled a more accurate analysis. Every data segment is structured in a
  \name{struct ByteRange} that contains all the relevant information. By storing
  the relative sequence numbers (first packet starts with sequence number 0) we
  \erratumReplace[type={grammar}, description={Improve intended
    meaning}]{could}{can} more easily compare the output with wireshark when
  debugging.
%
  \erratumReplace[type={grammar}, description={Improve grammar}]{We list here
    some}{Following is a list} of the variables from \name{struct ByteRange} and
  what feature they provide:

  \begin{enumerate}[itemindent=0mm,label=\large\textbullet,parsep=0pt]
    \bitem{packet_sent_count, packet_retrans_count and packet_received_count}
    giving a complete picture of the number of packets that were used to
    transfer this data.

    \bitem{data_sent_count, data_retrans_count, data_received_count, rdb_count}
    providing the context for each transmit as well as how many times the data
    arrived on the receiver side.

    \bitem{pcap timestamps for all the packets that sent this data segment}
    which makes it possible to use the correct timestamp when calculating the
    \gls{ACK-latency}. One might think that the correct timestamp is always from
    the last packet that sent the data, but the sender side may retransmit data
    even when it is not necessary, in which case using the timestamp from the
    last packet yields incorrect latencies.

    \bitem{tcp timestamps for all the sent packets and the packet that was first
      received.} This makes it possible to identify exactly which of the sent
    packets were received.

    \bitem{acked_sent, ack_count, dupack_count} to count the number of
    pure-\glspl{ACK} sent for this sequence number, the number of times this
    sequence number was \glsed{ACK} as well as the number of \glspl{dupACK}.

  \end{enumerate}

  \litem{Calculating RDB ``hits'' and ``misses''}
  The \name{ByteRange} also has the variables \code{rdb_byte_hits} and
  \code{rdb_byte_miss} that are used to count the \emph{hits} and \emph{misses}.
  When \gls{RDB} bundles data, \emph{hits} is the measure of how many of the
  \gls{RDB} packets that filled a hole on the data sequence on the receiver
  side. \emph{Misses} are the packets that contained bundled data which was
  already successfully delivered.

  \litem{Ability to produce data from only parts of the trace}

  \litem{Performance improvements} based on profiling with \name{valgrind}'s
  \name{cachegrind} tool as well as memory optimizations. The result is that
  \gls{analysetcp}, compared to earlier versions, performs much better both in
  terms of CPU time and memory usage, while producing more detailed data and
  statistics.

\end{enumerate}


\begin{command}[%
listing and comment, % Override default value (listing only)
listing file={include/commands/analysetcp.out},
%comment={Command},
label={command-analyseTCP},
]
\end{command}
\docaptionof{commandlisting}{Running \name{analyseTCP}}


\subsection{tcpproberdb}

\gls{tcpproberdb} is a kernel module we have written which is based on the
\gls{tcpprobe} module available in the \linuxkernel{}. This tool uses
kprobes to initiate a callback for each received \gls{ACK} to print different
properties of the socket, like \gls{CWND} and \glspl{PIF}.
\erratumNote[type={readability}]{Created paragraph}

kprobes \erratumReplace[type={grammar}, description={Fix typo}]{enables}{enable}
code in the kernel to dynamically attach a callback routine to be called before
entering a kernel function. This way of gathering the information should be more
efficient than calling \code{getsockopt} with \code{TCP_INFO} argument from user
space continually, and will also provide the information more consistently. The
kprobe trap is guaranteed to be called for every call to the kernel function,
while the \code{getsockopt} call will only return the information available at
the time the user mode process is scheduled. \Cref{code-bash-tcpprobe} shows the
script used before each test to load the module and capture the data. Example
output from \gls{tcpproberdb} can be seen in \cref{command-tcpproberdb}.

\begin{ccode}[%
listing and comment, % Override default value (listing only)
comment={The bash script used to fetch information of the TCP socket},
label={code-bash-tcpprobe},
listing options={style={bashstyle}, numbers=left},
]
#/bin/bash
PORT=${PORT:-0} # Default 0 == All
FULL=${FULL:-1} # On every ACK
FLUSH=${FLUSH:-1} # Flush on every packet
cd /root/tcp_probe_rdb
make reload V=1 PORT=$PORT FLUSH=$FLUSH FULL=$FULL
cat /proc/net/tcpproberdb > /root/tcpproberdb.out
\end{ccode}
\docaptionof{codelisting}{Bash script used to run \name{tcpproberdb}}


\begin{command}[%
listing and comment, % Override default value (listing only)
listing file={include/commands/tcpproberdb.out},
%comment={Command},
label={command-tcpproberdb},
]
\end{command}
\docaptionof{commandlisting}{Example output from \name{tcpproberdb}}


\subsection{Modifications to netem}
During the development of \gls{rdb2}, we modified \gls{netem} by adding a new
loss mode. By default, \gls{netem} supports specifying random packet loss.
%
\erratumReplace[type={correction}, description={Improve intended meaning}]{While
  this useful for experiments, it was not as useful while developing \gls{rdb2}.
  To easily reproduce a reliably and}{During the development of \gls{rdb2}, to
  reliably reproduce a}
%
simple loss pattern, we extended \gls{netem}
with a fixed loss mode. The patch for the \linuxkernel{} and \name{iproute2} can
be studied at \cref{sect:Netem-with-fixed-loss-patch}.
