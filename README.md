Download the [thesis PDF](https://bitbucket.org/bendikro/masters-thesis/src/7494948d25c5e183132e1be8a6bad8230a45d67b/thesis.pdf) submitted for publication at  [UiO](www.uio.no)

Download the [updated thesis-errata](https://bitbucket.org/bendikro/masters-thesis/src/master/thesis.pdf) containing corrections with an errata list
