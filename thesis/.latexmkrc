# -*- mode: perl -*- // For emacs
# for glossaries/acronyms
add_cus_dep('glo', 'gls', 0, 'run_makeglossaries');
add_cus_dep('acn', 'acr', 0, 'run_makeglossaries');

sub run_makeglossaries {
  my ($base_name, $path) = fileparse($_[0]);
  my $extra_args = $silent ? "-q" : "";
  system "echo \"makeglossaries with TEXINPUTS: \$TEXINPUTS\"";
  system "makeglossaries $extra_args -s main.xdy -d $path '$base_name'";
}

my $TEXINPUTS = defined($ENV{'TEXINPUTS'}) ? $ENV{'TEXINPUTS'} : '';
$ENV{'TEXINPUTS'}='./build/tikzcache/:' . $TEXINPUTS;

$ENV{'TEXINPUTS'}='./build/tikzcache/:' . $TEXINPUTS;
#$ENV{'TEXINPUTS'}='include/customtex/:' . $TEXINPUTS;
#$ENV{'TEXINPUTS'}='images/:images/tikz:' . $TEXINPUTS;

push @generated_exts, 'glo', 'gls', 'glg';
push @generated_exts, 'acn', 'acr', 'alg';
$clean_ext .= ' %R.ist %R.xdy';
$pdf_previewer = "start qpdfview";
$pdf_mode = 1;
$build_success_cmd="cp %D " . $ENV{'PWD'} . "/";
