\tikzset{font=\sffamily}
\definecolor{tcp}{RGB}{51, 122, 153}
\definecolor{ip}{RGB}{102, 153, 102}
\definecolor{free}{RGB}{150, 150, 150}
\definecolor{eth}{RGB}{204, 152, 0}
\colorlet{payload}{blue!20!white}

\begin{tikzpicture}[auto, on grid, node distance=0cm,%framed,%
scale=\textwidth/15.2cm,samples=200,
inner frame sep=0mm,
 comeback/.style={
  minimum height=1cm,
  minimum width=8mm,
  draw=black,
  text centered,
  anchor=west,
  outer sep=0pt,
  %text height=1.5ex,
  text depth=.0ex, % Necessary to align the text in the nodes...why?
%    text width=11em,
  },
%every node/.style={outer sep=0}
  ]

\begin{scope}%[yshift=0.15cm,xshift=-150]
\node[comeback,fill=eth]  (W0) {ETH};
\node[comeback,text width=1cm,fill=ip]  (W1) at (W0.east) {IP};
\node[comeback,text width=1cm,fill=tcp]  (W2) at (W1.east) {TCP};
\node[comeback,text width=3cm,fill=payload]  (W3) at (W2.east) {Payload (TCP)};
\node[comeback,text width=5.5cm,fill=free]  (W4) at (W3.east) {Unused Space};
\node[comeback,fill=eth]  (CRC) at (W4.east) {CRC};
\end{scope}
%
\coordinate (header1) at ([shift={(.1,.1)}] W0.north west);
\coordinate (header2) at ([shift={(-.1,.1)}] W2.north east);
%
\coordinate (eth1) at ([shift={(.1,.1)}] W0.north west);
\coordinate (eth2) at ([shift={(-.1,.1)}] W0.north east);
\coordinate (ip1) at ([shift={(.1,.1)}] W1.north west);
\coordinate (ip2) at ([shift={(-.1,.1)}] W1.north east);
\coordinate (tcp1) at ([shift={(.1,.1)}] W2.north west);
\coordinate (tcp2) at ([shift={(-.1,.1)}] W2.north east);
%
\coordinate (headersize1) at ([shift={(.1,.1)}] W0.north west);
\coordinate (headersize2) at ([shift={(-.1,.1)}] W2.north east);
%
\coordinate (payload1) at ([shift={(.1,.1)}] W3.north west);
\coordinate (payload2) at ([shift={(-.1,.1)}] W3.north east);
%
\coordinate (freespace1) at ([shift={(.1,.1)}] W4.north west);
\coordinate (freespace2) at ([shift={(-1,.1)}] W4.north);
\coordinate (freespace3) at ([shift={(-.1,.1)}] W4.north east);
%
\coordinate (ethernetmin1) at ([shift={(.1,-.1)}] W0.south west);
\coordinate (ethernetmin2) at ([shift={(-1,-.1)}] W3.south);
%
\coordinate (ethernetminfree1) at ([shift={(.1,-.1)}] W0.south west);
\coordinate (ethernetminfree2) at ([shift={(-1,-.1)}] W4.south);
%
\coordinate (ethernetmax1) at ([shift={(.1,-1.1)}] W0.south west);
\coordinate (ethernetmax2) at ([shift={(-.1,-1.1)}] CRC.south east);
%
\coordinate (ethcrc1) at ([shift={(.1,.1)}] CRC.north west);
\coordinate (ethcrc2) at ([shift={(-.1,.1)}] CRC.north east);
%
\begin{scope}
\draw [decorate,decoration={brace,amplitude=4pt},rotate=270] (eth1) -- (eth2) node [midway,yshift=1mm] {\footnotesize 14};
\draw [decorate,decoration={brace,amplitude=4pt},rotate=270] (ip1) -- (ip2) node [midway,yshift=1mm] {\footnotesize 20};
\draw [decorate,decoration={brace,amplitude=4pt},rotate=270] (tcp1) -- (tcp2) node [midway,yshift=1mm] {\footnotesize 20};
\draw [decorate,decoration={brace,amplitude=8pt},rotate=270] ([shift={(-.7,0)}]header1) -- ([shift={(-.7,0)}]header2) node [midway,yshift=2.5mm] {\footnotesize  Headers (54 bytes)};
\draw [decorate,decoration={brace,amplitude=6pt},rotate=270] (payload1) -- (payload2) node [midway,yshift=1.7mm] {\footnotesize  100 bytes};
\draw [decorate,decoration={brace,amplitude=8pt},rotate=270] ([shift={(0,0)}]freespace1) -- ([shift={(0,0)}]freespace3) node [midway,yshift=2.5mm] {\footnotesize  1360 bytes};
%\draw [decorate,decoration={brace,amplitude=6pt},rotate=270] (freespace1) -- (freespace2) node %[midway,yshift=1.7mm] {\footnotesize  358 bytes};

\draw [decorate,decoration={brace,amplitude=4pt},rotate=270] (ethcrc1) -- (ethcrc2) node [midway,yshift=1mm] {\footnotesize  4};

\draw [decorate,decoration={brace,amplitude=8pt}] (ethernetmin2) -- (ethernetmin1) node [midway,yshift=-3mm,xshift=10mm,align=right] {\footnotesize  64 bytes (Minimum ethernet frame size)};
\draw [decorate,decoration={brace,amplitude=8pt}] (ethernetmax2) -- (ethernetmax1) node [midway,yshift=-3mm,align=center] {\footnotesize 1518 bytes\\\footnotesize(Maximum ethernet frame size)};
\end{scope}
\end{tikzpicture}
