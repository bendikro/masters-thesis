\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel} % Multilingual support

\usepackage{morewrites} % Increases the silly max 16 streams in TeX that causes
                        % the error "No room for a new \write”

%\input{preamble} % Must use \input to make glossaries work

\usepackage[mode=buildnew,build={latexoptions={-interaction=batchmode -shell-escape -output-directory=build/tikzcache/}}]{standalone}

% Must add the images/tikz to the TEXINPUTS variable for
% standalone (call to pdflatex) to find the files
\makeatletter
\let\latexorig\sa@build@command
\renewcommand{\sa@build@command}{TEXINPUTS=.:images/tikz:\space\latexorig}
\makeatother


%\usepackage{epstopdf} % Automatically convert eps to pdf
%\epstopdfsetup{outdir=build/}
\graphicspath{{images/}{include/}{build/}{build/tikzcache/}{images/tikz/}%
  {include/ififorside/}{include/uioforside/}{include/patch/}%
  {images/latency-x-traffic-dpif/}}  % Search for figures in these directories
\makeatletter
\def\input@path{{images/tikz/}{include/}{include/customtex/}{include/ififorside/}{include/patch/}{build/}{build/tikzcache/}}
\makeatother

\usepackage{float}
\usepackage{placeins}   % Defines a \FloatBarrier command, beyond which floats may not pass;
\usepackage{newfloat}   % Used for the custom listings (listofcodelistings, listofcommands)

\usepackage{xparse}

\usepackage{pgfplots}
\pgfplotsset{compat=1.11}
\usepackage{pgfplotstable}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{backgrounds}


%\usepackage[noabbrev]{cleveref}[2013/12/28] % Provides \cref command (must be after hyperref)

%\include{colorbox} % Include colorbox definitions

\setbeamertemplate{navigation symbols}{}


\usepackage{beamerthemeshadow}
%\usepackage{beamerthemecustom}

\usepackage{subcaption} % Adds subfigure command to have multiple figures grouped together
\usepackage{setspace}

\usepackage{booktabs}   % Prettier tables (http://ctan.org/pkg/booktabs)
\usepackage{tabularx}   % Tabular table
\usepackage{multirow}

\usepackage[strings]{underscore} % Allow underscores in strings and filenames (must be after )

% Remove footline with author and title
\setbeamertemplate{footline}{}

\title{Taming Redundant Data Bundling}
\subtitle{Balancing fairness and latency for redundant bundling in TCP}

\date{March 13, 2015}

\author{Bendik Rønning Opstad}

\institute[]
{
  University of Oslo\\
  Department of Informatics
}

\subject{Computer Science}

\mode<presentation>
{
%  \setbeamertemplate{background canvas}[vertical shading][bottom=red!10,top=blue!10]
%  \usetheme{Warsaw}
  \usefonttheme[onlysmall]{structurebold}
}

\usepackage[defernumbers=true,
backend=biber,
%citestyle=authoryearnum,    % authoryearnum style shows the label number after year,
citestyle=authoryear,
bibencoding=utf8,
%backref=true,     % Added reference link back to the page of the ref command.
hyperref=true,    % Add PDF links
%autocite=footnote,
uniquename=init,
bibstyle=numeric,
sorting=none,
isbn=false,url=false,doi=false,eprint=false
]{biblatex}

\addbibresource{references.bib}

\setbeamertemplate{bibliography item}[triangle]

\newcommand{\subfigwidth}{0.8\textwidth}

\begin{document}

\frame{\titlepage}
%\begin{frame}
%  \titlepage
%\end{frame}

\section<presentation>*{Outline}

\begin{frame}{Outline}
  \tableofcontents[part=1,pausesections]
\end{frame}

%\AtBeginSubsection[]
\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
%    \tableofcontents[current,currentsubsection]
    \tableofcontents[current,currentsection]

%  \tableofcontents[part=1,pausesections]
  \end{frame}
}

\part<presentation>{Main Talk}

\section{Background}

%\begin{frame}
%  \frametitle{Outline}
%  %\tableofcontents[currentsection]
%\end{frame}

\subsection{TCP}

\begin{frame}{TCP}

\begin{itemize}
\item Most commonly used protocol on the Internet (\cite{Analysis-of-Internet-Backbone-Traffic-and-Header-Anomalies-Observed:2007}).
\item Stream oriented protocol, providing reliable, in-sequence delivery.
\item Optimized for greedy traffic (network limited)
\item Controls the transmission rate using a congestion control (CC) algorithm
\item The CC must be \emph{TCP fair}, measured as the share of allocated bandwidth.
\end{itemize}

\end{frame}


\subsection{Thin streams}

\begin{frame}{Thin streams}

How to define thin streams?

\pause

\vspace{5mm}

%Produced by applications with strict latency requirements, such as
%\vspace{3mm}
Characterized by:
\begin{itemize}
\item Small payloads
\item High inter-transmission time (ITT)
\item Application limited
\item Often produced by applications with strict latency requirements (such as
  online games (MMORGP), VoIP, and remote desktop control)
\item Few Packets in flight (PIF)
\end{itemize}

\end{frame}

\begin{frame}{Thin stream examples}

\newcommand{\protocolskip}{\hspace{1.5em}}
\newcommand{\payloadskip}{\hspace{1.7em}}
\newcommand{\ittskipmax}{\hspace{1.2em}}
\newcommand{\ittskip}{\hspace{1.7em}}


\begin{table}
\centering
\resizebox{\textwidth}{!}{%
%\renewcommand{\arraystretch}{1.2}  % Adjust space between rows
\setlength{\tabcolsep}{2pt} % Reduce space between column (Default 6pt)
\begin{tabular}{l@{\protocolskip}c@{\hspace{1em}}cc@{\payloadskip\hspace{1em}}cccc}
  % Top line
  %\toprule
  \multicolumn{1}{@{}c}{Application} &
  \multicolumn{3}{c@{\payloadskip}}{Payload size} &
  \multicolumn{4}{l}{Packet inter-arrival time (ms)}
\\
% Skip first two columns
%&
%% Skip the row below "Payload size (bytes)"
%\multicolumn{3}{c}{} &
%\multicolumn{4}{c}{}
%%\multicolumn{2}{c@{\ittskip}}{Percentiles}
%\\
% Skip first two columns
\multicolumn{1}{@{}c}{} &
%\multicolumn{1}{c@{\protocolskip}}{} &
\multicolumn{1}{c}{avg} & \multicolumn{1}{c}{min} & \multicolumn{1}{l}{max} &
\multicolumn{1}{@{\hspace{1.5em}}c}{avg} & \multicolumn{1}{c}{med} & \multicolumn{1}{c}{min} & \multicolumn{1}{c}{max}
%\multicolumn{1}{c}{1} %& \multicolumn{1}{c@{\ittskip}}{99}
\\
%
% Header end line
\cmidrule(lr){1-8}
%
\rule{0pt}{3ex}% Space before first row of data
Windows Remote Desktop          & 111 & 8  & 1417 & 318  & 159 & 1 & 12254  \\
VNC (from client)               & 8   & 1  & 106  & 34   & 8   & 0   & 5451   \\
VNC (from server)               & 827 & 2  & 1448 & 38   & 0   & 0   & 3557   \\
Skype (2 users)                 & 236 & 14 & 1267 & 34   & 40  & 0   & 1671   \\
SSH text session                & 48  & 16 & 752  & 323  & 159 & 0   & 76610  \\
Anarchy Online                  & 98  & 8  & 1333 & 632  & 449 & 7   & 17032  \\
World of Warcraft               & 26  & 6  & 1228 & 314  & 133 & 0   & 14855  \\
Age of Conan                    & 80  & 5  & 1460 & 86   & 57  & 0   & 1375   \\
\end{tabular}%
}% End resizebox
\caption{Examples of thin stream packet statistics (\cite[p.10]{petlund-2009})}%
\end{table}


\end{frame}

\begin{frame}{TCP + thin streams}

  Over TCP, thin streams experience high latencies due to how packet loss is
  handled.

\begin{itemize}
\item Fast retransmit when receiving three duplicate acknowledgments (dupACKs)
\item Retransmission timeout (RTO)
\end{itemize}

\begin{figure}
\begin{subfigure}[b]{\textwidth}
\includestandalone[width=\textwidth]{thin-stream-timeline-three-dupacks}
\end{subfigure}
%\\[7mm]
%\begin{subfigure}[b]{\textwidth}
%\includestandalone[width=\textwidth]{thin-stream-timeline-RTO}
%\caption{\Gls{fast-retransmit} is first triggered after an \gls{RTO} is triggered}
%\label{figure:thin-loss-RTO}
%\end{subfigure}
%\\[7mm]
%\begin{subfigure}[b]{\textwidth}
%\includestandalone[width=\textwidth]{thin-stream-timeline-thin-dupack}
%\caption{\Gls{fast-retransmit} triggered after one \gls{dupACK}}
%\label{figure:thin-loss-thin-dupack}
%\end{subfigure}
%\end{figurebox}
%\caption{Timelines showing when \gls{fast-retransmit} is triggered}
\end{figure}


\end{frame}


\begin{frame}{Why do thin streams suffer?}

\begin{itemize}
\item{TCP is optimized for throughput, and does not care (too much) about latency}
\pause
\item{Competing greedy streams have bad manners}
\pause
\item{TCP developers could care more about thin streams (Hint: Linux kernel devs)}
%\item<4>{item2}
\end{itemize}

\vspace{3mm}
\pause
Why TCP then?

\end{frame}

\subsection{TCP thin-stream mechanisms in the Linux kernel}

\begin{frame}{Thin-stream mechanisms}

Thin-stream mechanisms implemented in the Linux kernel:
\begin{itemize}
\item Thin Linear Timeouts (LT)
\item Modified fast retransmit (mFR)
\item Early Retransmit
\item Tail Loss Probe
\end{itemize}

\vspace{5mm}
Common goal of reducing the delay before triggering
retransmissions. This is achieved by changing how the TCP engine \emph{reacts}
to anticipated or confirmed packet loss.

\vspace{2mm}
Active only in certain (thin-stream) scenarios.


\end{frame}

\subsection{Preliminary experiments}

\renewcommand{\subfigwidth}{0.5\textwidth}

\begin{frame}{Preliminary experiments}

\begin{itemize}
\item 10 greedy streams
\item 21 thin streams with ITT: 100:15 ms and payload 120 bytes
\item RTT: 150
\item Rate limit 5Mbit with Pfifo queue of length 60
\end{itemize}

\begin{figure}
\centering
\includegraphics[width=0.6\textwidth,page=1]{thin-stream-mod-CDF/RDBSENDER_ZSENDER_TEST_THIN_STREAM_MOD_latency_itt100_qlen60_CDF_21vs10_ggplot_eretr_queueing_delay}
\end{figure}
\end{frame}


\begin{frame}{Preliminary experiments}

Two problems that none of the mentioned thin-stream mechanisms fix
\begin{itemize}
\item Latency is still increased by one RTT at a minimum.
\item Head-of-line blocking, causing multiple packets to be held back on the receiver side.
\end{itemize}


\begin{figure}

\begin{subfigure}[b]{\subfigwidth}
\includegraphics[width=\textwidth,page=1]{thin-stream-mod-CDF/RDBSENDER_ZSENDER_TEST_THIN_STREAM_MOD_latency_itt100_qlen60_CDF_21vs10_ggplot_eretr}
\caption{No thin-stream mechanisms}
\end{subfigure}%\subfighorizontalspacing{}
%
\begin{subfigure}[b]{\subfigwidth}
\includegraphics[width=\textwidth,page=8]{thin-stream-mod-CDF/RDBSENDER_ZSENDER_TEST_THIN_STREAM_MOD_latency_itt100_qlen60_CDF_21vs10_ggplot_eretr}
\caption{With mFR, LT and ER+TLP}
\end{subfigure}
\end{figure}

\end{frame}


\section{Redundant data bundling}

\begin{frame}{Redundant data bundling}
  Differs from the existing mechanisms by taking a \emph{proactive} approach to
  dealing with (potential) packet loss.

\begin{itemize}
\item Modify the sender side to bundle already sent data with existing packets.
\item A form of retransmission mechanism (though much more aggressive)
\item First RDB prototype (RDBv1) implemented in Linux kernel v2.6.22
  (\cites{evensen-2008}{petlund-2009}).
\end{itemize}


\end{frame}

\subsection{Concept}

\begin{frame}
  \frametitle{Utilizing the ``free'' space in ethernet frames}

  Free, in this context, means space that can be used without producing more
  ethernet frames on the wire.

\begin{figure}[h]
\centering
\includestandalone[width=0.9\textwidth]{ethernet-frame}
\end{figure}

\end{frame}

\begin{frame}{Example of RDB packets}

  Example with four separate data segments (S1-S4) showing how RDB organizes the
  data in each packet.

\renewcommand{\subfigwidth}{0.7\textwidth}
\newcommand{\subfigsep}{\vspace{3mm}}
\centering
\begin{figure}
\centering
\begin{subfigure}{\subfigwidth}
\includestandalone[width=\textwidth]{tcp-packet-frame}
\end{subfigure}%
\subfigsep{}
\begin{subfigure}[b]{\subfigwidth}
\includestandalone[width=\textwidth]{rdb-packet-frame}
\end{subfigure}%
\subfigsep{}
\begin{subfigure}[b]{\subfigwidth}
\includestandalone[width=\textwidth]{rdb-packet-frame2}
\end{subfigure}%
\subfigsep{}
\begin{subfigure}[b]{\subfigwidth}
\includestandalone[width=\textwidth]{rdb-packet-frame3}
\end{subfigure}%
\end{figure}

\end{frame}

\subsection{RDB prototype v1}
\begin{frame}{RDBv1}
  Experiments on RDBv1 showed promising results, but some concerns were raised.

\begin{itemize}
\item Too aggressive (no limitation on when to bundle)
\item Hiding loss events, preventing the TCP engine from adjusting the send rate.
\item The implementation is to obtrusive to the TCP output engine.
\end{itemize}

\begin{figure}%[H]
\centering
\resizebox{0.5\textwidth}{!}{\includestandalone{tcp-output-queue} }
\caption[The TCP output queue]{The TCP output queue}
\end{figure}

\end{frame}


\renewcommand{\subfigwidth}{0.45\textwidth}

\begin{frame}{RDBv1 - Preliminary fairness experiment}

\begin{itemize}
\item 6 Isochronous (Thick) streams with ITT 10, and payload 400
\item 6 greedy stream
\end{itemize}

\begin{columns}
  \begin{column}{.49\textwidth}
    \begin{figure}[h]
      \centering
      \includegraphics[width=\textwidth,page=2]{initial-tests/RDBSENDER_ZSENDER_TEST_THIN_RDBMISUSE_NvsN_throughput_ggplot}
    \end{figure}
  \end{column}

  \begin{column}{.49\textwidth}
    \begin{figure}[h]
      \centering
      \includegraphics[width=\textwidth,page=1]{initial-tests/RDBSENDER_ZSENDER_TEST_THIN_RDBMISUSE_NvsN_throughput_ggplot}
    \end{figure}
  \end{column}
\end{columns}

\end{frame}


\subsection{RDB prototype v2}

%\subsubsection{RDBv2 - Improving RDB}

\begin{frame}{Improving RDB}

\begin{itemize}
\item Ensure TCP fairness by detecting (hidden) loss events
\item Impose limitation on when to allow bundling
\item Reduce the oscillation of the send rate for thin streams
\item Re-implement RDB without modifying the buffers in the output queue
\end{itemize}

\end{frame}

\newcommand{\code}[1]{{\ttfamily \hyphenchar\font=`- {#1}}}



%\subsubsection{RDBv2 - When to allow bundling}

\begin{frame}{When to allow bundling}

LT and mFR use \code{tcp_stream_is_thin} (PIF \textless{} 4).

\vspace{2mm}
But what does it really test?

\begin{itemize}
\pause
\item Does it test if the stream is thin?
\pause
\item How does it know how often the application writes data?
\pause
\item Low RTT streams are favorized
\end{itemize}

\pause
\vspace{2mm}
A more precise name would be \code{can_trigger_fast_retransmit_within_one_rtt}

\end{frame}


\begin{frame}{When to allow bundling}

Re-thinking how to implement \code{tcp_stream_is_thin}

\begin{itemize}
\pause
\item A static (hardcoded) PIF limit is not a good \emph{general} thin-stream indicator
\item A dynamic way to more precisely identify if a stream as thin enough
\end{itemize}

\pause
\vspace{2mm}
Our solution: Calculate dynamic PIF limit using the RTT and ITT

\end{frame}

\begin{frame}{Dynamic PIF limit}

\begin{equation}
%\begin{split}
  DPIFL \;=\; \frac{RTT_{min}}{ITT_{min}}
%\end{split}
\end{equation}


\begin{figure}
\resizebox{0.8\textwidth}{!}{\includestandalone{pif-thin-streams} }
%\caption{The \acrshort{DPIFL} with minimum \acrshortpl{ITT} \ms{10} and \ms{20},
%  for \acrshortpl{RTT} in range $10 - 160$}
\end{figure}
\end{frame}


%\subsubsection{RDBv2 - Detecting packet loss}

\begin{frame}{Detecting packet loss with RDB}

  \pause

\begin{columns}
  \begin{column}{.49\textwidth}
    \begin{figure}[h]
      \centering
      \includegraphics[width=0.7\textwidth]{jonassm_rdb_timeline_loss}\
      \caption{TCP stream with RDB (\cite[p. 32]{markussen-2014})}
    \end{figure}
  \end{column}

  \begin{column}{.49\textwidth}
    What is the problem?
    \begin{itemize}
      \pause
    \item Sender host will not receive dupACKs after loss
      \pause
    \item Delayed ACKs will cause problems
      \pause
    \item Update: Delayed ACKs will \emph{not} cause problems. (Only delay ACKs when in-sequence)
    \end{itemize}
  \end{column}
\end{columns}

\end{frame}


\begin{frame}{Detecting packet loss with RDB}

  \begin{itemize}
    \pause
  \item We can expect one ACK for each RDB packet sent
    \pause
  \item If an ACK covers multiple packets, something is amiss
    \pause
  \item If a dupACK is received, something is definitely not going as planned
    \pause
  \item How to differentiate between reorder and loss?
  \end{itemize}

\end{frame}


\renewcommand{\subfigwidth}{0.5\textwidth}

\begin{frame}{Detecting packet loss with RDB}

\begin{columns}
  \begin{column}{.49\textwidth}
    \begin{figure}[h]
      \centering
      \includestandalone[width=\textwidth]{tcp-dsack-rdb}
      \caption{DSACK - Expected behavior}
    \end{figure}
  \end{column}

  \begin{column}{.49\textwidth}
    \pause
    \begin{figure}[b]
      \centering
      \includestandalone[width=\textwidth]{tcp-dsack-rdb-reorder}
      \caption{DSACK - Reordering}
    \end{figure}%
  \end{column}
\end{columns}


\end{frame}


%\subsection{RDBv2 - }


\begin{frame}{Congestion Control}

  To give thin streams a smoother send rate we implemented a new CC based on TCP
  Friendly Rate Control (TFRC).

\begin{itemize}
\pause
\item A CC for non-TCP protocols to ensure TCP-Friendliness
\item Equation based CC (Throughput is calculated from a formula)
\item Designed to avoid the drastic reduction of the CWND on loss events.
\end{itemize}

\end{frame}


\begin{frame}{Congestion Control - TFRC}

\begin{itemize}
\item Maintain a loss history of 8 loss intervals (buckets)
\item Calculate a loss event rate based on the loss history
\item Calculate the allowed send rate using the throughput formula
\item Convert into CWND value
\end{itemize}

\end{frame}


\begin{frame}{TFRC simulations}

\begin{figure}
\centering
\includegraphics[width=\textwidth,page=1]{tfrc_CWND_loss}
\end{figure}
\end{frame}

\begin{frame}{Testbed experiment results for comparison}
\begin{figure}
\centering
\includegraphics[width=\textwidth,page=1]{CWND_NETEM_LOSS}
\end{figure}

\end{frame}


\DeclareDocumentCommand{\loadfigure}{O{\textwidth} m} {\pgfkeys{{#2}={#1}}}

\input{experiments/latency-netem-pif-latex_figures_page_list}
\input{experiments/latency-x-traffic-dpif-latex_figures_page_list}
\input{experiments/latency-x-traffic-dpif-rdblim-latex_figures_page_list}
\input{experiments/misuser-dpif-tfrcsp-latex_figures_page_list}


\section{Evaluation}

\subsection{Metrics}

\begin{frame}{Metrics}

\pause

Main metrics:

\begin{itemize}
\item ACK-Latency (Application layer latency)
\item Effect on competing traffic (TCP Friendliness)
\end{itemize}

\pause

Secondary metrics:

\begin{itemize}
\item ITT
\item Payload size
\item CWND
\item Number of packets sent
\end{itemize}

They are all connected, and together they give a more complete picture.

\end{frame}

\subsection{Experiments}

\begin{frame}{Experiments}

Experiment 1 (Latency):
\begin{itemize}
\item Uniform loss.
\item Show how head-of-line blocking is reduced
\end{itemize}

Experiment 2 and 3 (Latency in realistic scenario)
\begin{itemize}
\item Two hosts sending thin streams
\item Greedy cross traffic to cause congestion.
\item With and without limitation on how much to bundle
\end{itemize}

Experiment 4 (Fairness / Potential of abuse)
\begin{itemize}
\item Two hosts sending isochronous traffic (400 bytes with short ITT)
\item One host sending greedy traffic
\end{itemize}

\end{frame}


\begin{frame}{Testbed}
\begin{figure}
\includestandalone[width=0.8\textwidth]{testbed}
%\caption{Testbed network setup}
\end{figure}
\end{frame}

\renewcommand{\subfigwidth}{0.6\textwidth}

\subsection{Key results}

\begin{frame}{Key results - Experiment 1}

\begin{itemize}
\item Uniform loss (netem)
\item No competing traffic -\textgreater{} No queuing delay
\item Static PIF limit
\end{itemize}

  \begin{figure}
% %   \captionsetup{skip=0pt}
%    \centering
    \begin{subfigure}[b]{\subfigwidth}
      \loadfigure{/latency-netem-pif-Streams:20-Payload:120-ITT:30:3-RTT:150-5-1-1-Loss:10}
    \end{subfigure}
%%    \caption[Experiment 1: Loss 10\% ITT 30:3]{}
  \end{figure}

\end{frame}


\begin{frame}{Key results - Experiment 2}

\begin{itemize}
\item Three sender hosts (Two thin-stream senders, one sending greedy streams)
\item Static PIF limit
\end{itemize}

\renewcommand{\subfigwidth}{0.48}
\newcommand{\subfighorizontalspacing}{\hspace{0.1cm}}
\begin{figure}
  \captionsetup{font=small,skip=-5pt}
  \centering
  \centering%
  \begin{subfigure}[b]{\subfigwidth\textwidth}
    \loadfigure{/latency-x-traffic-dpif-Streams:5-Streams-Greedy:5-Payload:120-ITT:10:1-RTT:150-DUR:5-NUM:0-PIFG:PIF0-DPIF0-TFRC0-DU0-LT0-ER:3-QLEN:63-CONG2:rdb-Plot-type:Latency}
    \caption{\code{E2-5-10:1-TCP}}
  \end{subfigure}%
  \begin{subfigure}[b]{\subfigwidth\textwidth}
    \loadfigure{/latency-x-traffic-dpif-Streams:5-Streams-Greedy:5-Payload:120-ITT:10:1-RTT:150-DUR:5-NUM:0-PIFG:PIF0-DPIF10-TFRC0-DU0-LT0-ER:3-QLEN:63-CONG2:rdb-Plot-type:Latency}
    \caption{\code{RDB-5-10:1}}
  \end{subfigure}%
  \end{figure}
\end{frame}


\begin{frame}{Key results - Experiment 2}

\begin{itemize}
\item The devil is in the detail (the tables in this case)
\end{itemize}

\renewcommand{\subfigwidth}{0.48}
\newcommand{\subfighorizontalspacing}{\hspace{0.1cm}}
\begin{figure}
  \captionsetup{font=small,skip=-5pt}
  \centering
  \centering%
  \begin{subfigure}[b]{\subfigwidth\textwidth}
    \loadfigure{/latency-x-traffic-dpif-Streams:5-Streams-Greedy:5-Payload:120-ITT:10:1-RTT:150-DUR:5-NUM:0-PIFG:PIF0-DPIF0-TFRC0-DU0-LT0-ER:3-QLEN:63-CONG2:rdb-Plot-type:Goodput}
    \caption{\code{E2-5-10:1-TCP}}
  \end{subfigure}%
  \begin{subfigure}[b]{\subfigwidth\textwidth}
    \loadfigure{/latency-x-traffic-dpif-Streams:5-Streams-Greedy:5-Payload:120-ITT:10:1-RTT:150-DUR:5-NUM:0-PIFG:PIF0-DPIF10-TFRC0-DU0-LT0-ER:3-QLEN:63-CONG2:rdb-Plot-type:Goodput}
    \caption{\code{RDB-5-10:1}}
  \end{subfigure}%
  \end{figure}
\end{frame}


\begin{frame}{Key results - Experiment 2}

\begin{itemize}
\item The devil is in the detail (the tables in this case)
\end{itemize}

\renewcommand{\subfigwidth}{0.48}
\newcommand{\subfighorizontalspacing}{\hspace{0.1cm}}
\begin{figure}
  \captionsetup{font=small,skip=-5pt}
  \centering
  \centering%
  \begin{subfigure}[b]{\subfigwidth\textwidth}
    \loadfigure{/latency-x-traffic-dpif-Streams:5-Streams-Greedy:5-Payload:120-ITT:10:1-RTT:150-DUR:5-NUM:0-PIFG:PIF0-DPIF0-TFRC0-DU0-LT0-ER:3-QLEN:63-CONG2:rdb-Plot-type:Throughput}
    \caption{\code{E2-5-10:1-TCP}}
  \end{subfigure}%
  \begin{subfigure}[b]{\subfigwidth\textwidth}
    \loadfigure{/latency-x-traffic-dpif-Streams:5-Streams-Greedy:5-Payload:120-ITT:10:1-RTT:150-DUR:5-NUM:0-PIFG:PIF0-DPIF10-TFRC0-DU0-LT0-ER:3-QLEN:63-CONG2:rdb-Plot-type:Throughput}
    \caption{\code{RDB-5-10:1}}
  \end{subfigure}%
  \end{figure}
\end{frame}



\begin{frame}{Key results - Experiment 3}

\begin{columns}
  \begin{column}{.49\textwidth}
    \begin{figure}[h]
      \centering
      \loadfigure{/latency-x-traffic-dpif-rdblim-Streams:5-Streams-Greedy:5-Payload:120-ITT:10:1-RTT:150-DUR:5-NUM:0-PIFG:PIF0-DPIF0-TFRC0-DU0-LT0-ER:3-QLEN:63-CONG2:rdb-Plot-type:Goodput}
     \caption{E3-5-10:1-TCP}
    \end{figure}
  \end{column}

  \begin{column}{.49\textwidth}
    \begin{figure}[b]
      \centering
     \loadfigure{/latency-x-traffic-dpif-rdblim-Streams:5-Streams-Greedy:5-Payload:120-ITT:10:1-RTT:150-DUR:5-NUM:0-PIFG:PIF0-DPIF10-TFRC0-DU0-LT0-ER:3-QLEN:63-CONG2:rdb-Plot-type:Goodput}
      \caption{E3-5-10:1-RDB}
    \end{figure}%
  \end{column}

\end{columns}
\end{frame}


\begin{frame}{Key results - Experiment 3}

\begin{columns}
  \begin{column}{.49\textwidth}
    \begin{figure}[b]
      \centering
      \loadfigure{/latency-x-traffic-dpif-rdblim-Streams:5-Streams-Greedy:5-Payload:120-ITT:10:1-RTT:150-DUR:5-NUM:0-PIFG:PIF0-DPIF10-TFRC1-DU0-LT0-ER:3-QLEN:63-CONG2:rdb-Plot-type:Goodput}
      \caption{E3-5-10:1-RDB-TFRC}
    \end{figure}%
  \end{column}

  \begin{column}{.49\textwidth}
    \begin{figure}[b]
      \centering
     \loadfigure{/latency-x-traffic-dpif-rdblim-Streams:5-Streams-Greedy:5-Payload:120-ITT:10:1-RTT:150-DUR:5-NUM:0-PIFG:PIF0-DPIF10-TFRC0-DU0-LT0-ER:3-QLEN:63-CONG2:rdb-Plot-type:Goodput}
      \caption{E3-5-10:1-RDB}
    \end{figure}%
  \end{column}

\end{columns}
\end{frame}







\begin{frame}{Key results - Experiment 3}

\begin{columns}
  \begin{column}{.49\textwidth}
    \begin{figure}[h]
      \centering
      \loadfigure{/latency-x-traffic-dpif-rdblim-Streams:5-Streams-Greedy:5-Payload:120-ITT:10:1-RTT:150-DUR:5-NUM:0-PIFG:PIF0-DPIF0-TFRC0-DU0-LT0-ER:3-QLEN:63-CONG2:rdb-Plot-type:Latency}
     \caption{E3-5-10:1-TCP}
    \end{figure}
  \end{column}

  \begin{column}{.49\textwidth}
    \begin{figure}[b]
      \centering
     \loadfigure{/latency-x-traffic-dpif-rdblim-Streams:5-Streams-Greedy:5-Payload:120-ITT:10:1-RTT:150-DUR:5-NUM:0-PIFG:PIF0-DPIF10-TFRC0-DU0-LT0-ER:3-QLEN:63-CONG2:rdb-Plot-type:Latency}
      \caption{E3-5-10:1-RDB}
    \end{figure}%
  \end{column}

\end{columns}
\end{frame}


\begin{frame}{Key results - Experiment 3}

\begin{columns}
  \begin{column}{.49\textwidth}
    \begin{figure}[b]
      \centering
      \loadfigure{/latency-x-traffic-dpif-rdblim-Streams:5-Streams-Greedy:5-Payload:120-ITT:10:1-RTT:150-DUR:5-NUM:0-PIFG:PIF0-DPIF10-TFRC1-DU0-LT0-ER:3-QLEN:63-CONG2:rdb-Plot-type:Latency}
      \caption{E3-5-10:1-RDB-TFRC}
    \end{figure}%
  \end{column}

  \begin{column}{.49\textwidth}
    \begin{figure}[b]
      \centering
     \loadfigure{/latency-x-traffic-dpif-rdblim-Streams:5-Streams-Greedy:5-Payload:120-ITT:10:1-RTT:150-DUR:5-NUM:0-PIFG:PIF0-DPIF10-TFRC0-DU0-LT0-ER:3-QLEN:63-CONG2:rdb-Plot-type:Latency}
      \caption{E3-5-10:1-RDB}
    \end{figure}%
  \end{column}

\end{columns}
\end{frame}




\begin{frame}{Key results - Experiment 4}

Barely able to bundle in low-ITT scenarios

\begin{columns}
  \begin{column}{.49\textwidth}
    \begin{figure}[b]
      \centering
      \loadfigure{/misuser-dpif-tfrcsp-Streams:7-Streams-Greedy:7-Payload:400-ITT:15:1-RTT:150-DUR:5-NUM:0-PIFG:PIF0-DPIF10-TFRC0-DU0-LT0-ER:3-QLEN:63-CONG2:rdb-Plot-type:Throughput}
      \caption{E4-7-15:1-RDB}
    \end{figure}%
  \end{column}

  \begin{column}{.49\textwidth}
    \begin{figure}[b]
      \centering
      \loadfigure{/misuser-dpif-tfrcsp-Streams:7-Streams-Greedy:7-Payload:400-ITT:15:1-RTT:150-DUR:5-NUM:0-PIFG:PIF0-DPIF10-TFRC1-DU0-LT0-ER:3-QLEN:63-CONG2:rdb-Plot-type:Throughput}
      \caption{E4-7-15:1-RDB-TFRC}
    \end{figure}%
  \end{column}

\end{columns}
\end{frame}


\begin{frame}{Key results - Experiment 4}

When able to bundle, there is nothing to gain throughput-wise.

\begin{columns}
  \begin{column}{.49\textwidth}
    \begin{figure}[b]
      \centering
      \loadfigure{/misuser-dpif-tfrcsp-Streams:7-Streams-Greedy:7-Payload:400-ITT:30:3-RTT:150-DUR:5-NUM:0-PIFG:PIF0-DPIF10-TFRC0-DU0-LT0-ER:3-QLEN:63-CONG2:rdb-Plot-type:Throughput}
      \caption{E4-7-30:3-RDB}
    \end{figure}%
  \end{column}

  \begin{column}{.49\textwidth}
    \begin{figure}[b]
      \centering
      \loadfigure{/misuser-dpif-tfrcsp-Streams:7-Streams-Greedy:7-Payload:400-ITT:30:3-RTT:150-DUR:5-NUM:0-PIFG:PIF0-DPIF10-TFRC1-DU0-LT0-ER:3-QLEN:63-CONG2:rdb-Plot-type:Throughput}
      \caption{E4-7-30:3-RDB-TFRC}
    \end{figure}%
  \end{column}

\end{columns}
\end{frame}


\section{Conclusion}

\begin{frame}{Conclusion}

What is most important?

\begin{itemize}
\item Limiting when to bundle (only on thin streams)
\item Properly reacting to loss (no hidden loss events)
\item Limiting the amount of data to bundle with each packet
\end{itemize}

\pause

Important findings:

\begin{itemize}
\item RDB is (reasonably) fair when reacting to loss
\item With RDB streams, the TFRC CC provides slightly higher CWND than pure TCP Reno in Linux kernel v3.15
\item Limiting the amount of data to bundle with each packet affects the loss rate (in the experiments)
\end{itemize}

\end{frame}

%\section{References}

%\subsection{References}

%\frametitle{References}

%\begin{frame}[allowframebreaks]{Bibliography}
\begin{frame}{References}
%  \nocite{*}
  \begingroup
  \renewcommand*{\bibfont}{\tiny}
  \setstretch{0.8}
  \linespread{0.9}
  \setbeamertemplate{bibliography item}[triangle]
  \setlength\bibitemsep{-0pt}
%  \setlength{\biblabelsep}{-0.5cm}

\printbibliography[
heading=none,
%\printbiblist[%
%heading=subbibintoc,
%prefixnumbers={a},
title={TReferences}]
\endgroup
\end{frame}

%\AtBeginSubsection{}

\end{document}
