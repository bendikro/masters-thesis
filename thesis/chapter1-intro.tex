\chapter{Introduction}\label{chap:intro}

Today's public Internet has had a tremendous growth, from the first ideas of a
global network in the 1960s, to the birth of TCP/IP in the 1980s, eventually
leading to an explosion in the number of users starting in the mid-1990s
continuing till today \citep{internet-growth}.

\Gls{TCP} \citerfcp{rfc793}, the most common protocol used on the Internet today
\citep{Analysis-of-Internet-Backbone-Traffic-and-Header-Anomalies-Observed:2007},
has mechanisms to control the send rate to prevent users from overflowing the
network with too much data. V. Jacobson's work on \gls{CC} for \gls{TCP} in the
late 1980s is, by many, recognized as a primary reason that enabled the Internet
to grow at such a speed and size as it has
\citep{Revisiting-inter-flow-fairness:2008,binomial-congestion-control-algorithms-2001}.

The development of \glspl{CC} and retransmission mechanisms for \gls{TCP} has
mainly focused on stability (fairness) and handling the transfers of the bigger
and bigger amounts of data through the network, i.e., \gls{throughput}
\citep{Multimedia-unfriendly-TCP-Congestion-Control-and-Home-Gateway-Queue-Management:2011}.
This has left interactive applications that value latency over \gls{throughput}
in a bad spot. Many of the applications with strict latency requirements produce
network traffic with \glsuseri{thin-stream} characteristics, meaning they send
smaller and fewer packets compared to greedier streams. Due to the design of the
mechanisms that are designed to prevent \gls{congestion-collapse}, such
interactive applications suffer from higher latencies - we argue unnecessarily,
or unfairly. In this thesis we focus on how to improve the performance for these
types of applications.

\section{Background and motivation}\label{sect:introduction-motivation}

Alternative transport protocols to \gls{TCP} and \gls{UDP} have emerged, that
aim to replace them for certain uses. An example is \gls{SCTP}, which should be
ideal for services that find \gls{UDP} to be too basic, but do not require or
want all of the functionality that \gls{TCP} provides. With \gls{SCTP}, ordering
is optional, which eliminates the issue of \gls{HOLB} in \gls{TCP}, which is
beneficial for many types of interactive applications. With a variety of
optionally negotiable features it could have the potential of replacing both
\gls{UDP} and \gls{TCP} for many use cases.

A major problem is that many firewalls in home gateways and middleboxes, only
support \gls{UDP} and \gls{TCP}, and do not let protocols such as \gls{SCTP}
through \citerfcp{rfc3257}. Also, while OSes such as Linux, Solaris and FreeBSD
and have \gls{SCTP} included, Windows and OSX do not have native
implementations. Until protocols such as \gls{SCTP} are better supported in
major OSes, home gateways, and middleboxes, they can not be used for services
whose traffic must pass through firewalls and \glspl{NAT}.

This gives a ``Chicken or the egg'' situation, where alternative protocols
cannot gain wide adoption without better support by the network nodes.
Meanwhile, manufacturers of operating systems and network middleboxes seem
unwilling to invest resources into supporting new protocols until they are
forced by the consumers.

A big challenge with deploying improvements to the Internet is the lack of any
centralized control of all the nodes. As changes to nodes in the Internet must
not break existing functionality, updates to protocols such as \gls{TCP} must be
backwards compatible. This lays heavy restrictions on what kind of changes that
can be made to \gls{TCP}.

Even with a clear trend showing an increase of audio and video streaming
traffic, studies on the ratio of $\frac{UDP}{TCP}$ could not find a clear
systematic trend showing a relative increase of \gls{UDP} usage at the expense
of \gls{TCP}
\citep{Media-Streaming-Observations:Trends-in-UDP-to-TCP-Ratio:2010}. This
suggests that the common belief that \gls{UDP} would be the obvious choice for
streaming services might not be correct.

As \gls{TCP} is still widely used, and is the \emph{de facto} standard for many
services that could benefit from the better latency provided by other protocols,
we wish to look at how to improve the latency for such services using \gls{TCP}.

\Glspl{thin-stream} using \gls{TCP} suffer from high latencies caused by the
in-order guarantee that \gls{TCP} provides. When packets are lost, the
mechanisms for retransmitting the lost data cause considerable delays. Any data
transmitted after the lost segment are subjected to \gls{HOLB} which means that
multiple data segments may be delayed due to only one lost packet.

In this thesis we present the continued work on the sender side \gls{TCP}
modification \gls{RDB} for the \linuxkernel{}. By enabling a more aggressive
(re)transmission mode for \glspl{thin-stream}, the per-packet latencies can be
considerably improved. The modifications are made to maintain compatibility with
\gls{TCP}, which should allow for easy deployment into existing networks.


\section{Problem statement}\label{sect:introduction-problem-statement}

The Internet is a packet-switched network providing best-effort delivery of data
packets. One of its strengths lies in how easily extendable the network is, and
how robust the transfer of data is when one node in the network goes down. A
weakness is that it heavily relies on the users to behave in a good manner.
There is no centralized governance controlling how users behave, that can
reprimand users that do not follow the ``rules'', partly because there really
are no rules.

This is why controlling the traffic using an end-to-end \gls{CC} is important.
Due to the lack of any such mechanisms in \gls{UDP}, trying to improve the
performance for time-dependent traffic on protocols utilizing \glspl{CC} is the
best solution for the network users as a whole. This will help the users that
currently use \gls{TCP} for such traffic, and reduce the incentives for
application developers to choose \gls{UDP} over \gls{TCP}.

Mechanisms have been developed to improve the situation for interactive
applications, one of them being \gls{RDB}, which works by piggy-backing
(bundling) already sent data in packets with new (unsent) data. The first
\gls{RDB} implementation, which we refer to as \gls{rdb1}, showed
great potential on improving the latency for \glspl{thin-stream}, but it left
certain issues unanswered. Uncertainties remain about the fairness of the
\gls{RDB} mechanism towards competing traffic. The lack of any mechanisms to
limit aggressiveness, as well as the potential for abuse, is not sufficiently
addressed in \gls{rdb1}, and is laid out as potential future work in
the conclusion \citep{evensen-2008}.

An implementation specific issue with \gls{rdb2} also remains,
regarding how the data contained in the \glspl{SKB} of the
\gls{tcp-output-queue} are modified by the mechanism. The operations required to
perform the manipulations of the \glspl{SKB} were deemed too intrusive in
regards to data integrity.

Based on the earlier work on \gls{RDB}, the goal of this thesis is to continue
the study of improving the latency for \gls{thin-stream} traffic generated by
interactive applications over \gls{TCP}. With the previously mentioned issues of
\gls{rdb1} in mind, we specifically aim to:

\begin{enumerate}[itemindent=0mm,label=\large\textbullet,parsep=0pt]

\item{Develop an \gls{RDB} implementation that is less intrusive than
    \gls{rdb1}, both in regards to data integrity of the buffers in
    the \gls{tcp-output-queue}, and to the \linuxkernel{}'s \gls{tcp-engine}
    code. Streamlining the implementation, by better organizing the code into
    separated logical segments, is important to simplify the work of developing
    and extending the functionality as well as to make future patch submissions
    feasible.}

\item{Investigate how to detect packet loss that is hidden from the current
    \gls{TCP} implementation due to the redundant data introduced by \gls{RDB}.}

\item{Evaluate mechanisms that limit redundant data bundling to situations where
    it is most needed. This is to balance the aggressiveness of \gls{RDB} against
    latency gains.}

\item{Ensure that streams utilizing \gls{RDB} are TCP-fair.}

\end{enumerate}


\section{Research Method}\label{sect:introduction-method}

The work in this thesis follows the \emph{design} paradigm described in
\emph{Computing As a Discipline} by the ACM Task Force
\citep{Computing-As-a-Discipline:1989}. This entails the process of stating the
requirements (1) and specification (2) for the system we intend to create,
before we design and implement the system (3), followed by evaluating (4).

We have written a prototype implementation of \gls{RDB} in the \linuxkernel{},
referred to as \gls{rdb2}, and experimentally evaluated the mechanism
in a lab testbed, with a focus on the problems addressed in this thesis. We then
analyse and present the results based on traffic traces and run time information
from the hosts on a multitude of different test setups.


\paragraph{Experiments}

The experiments are performed in a testbed consisting of hosts running Debian
Linux. We have set up the experiments with different configurations of sender
hosts to test how the \gls{rdb2} mechanism works in different scenarios.

We have run a set of latency tests with uniform loss rate enforced by
\gls{netem} to isolate the changes to the latency results by avoiding any
external influence by competing network streams. The next set of experiments are
set up with competing greedy and \gls{thin-stream} traffic to create a more
realistic network environment, as well as to see how the \gls{RDB} mechanism
affects the other network streams. The last set of experiments, which we call
fairness experiments, are designed to test how network streams produced by
\gls{rdb2} behave towards competing streams in respect to fairness
when potential ``evil-doers'' try to abuse the mechanism to (unfairly) gain
advantages over other competing network streams.


\paragraph{Data analysis}

We have analysed the problem area in order to identify suitable metrics by which
to evaluate the mechanisms. Then we have analysed the results of the experiments
using these metrics.
We calculate the \gls{ACK-latency}, the per-packet latency for the \gls{TCP}
streams, and compare the results of the different network streams. By
calculating the \gls{goodput} and \gls{throughput} from the packet traces, we
can compare the amount of data that the competing streams transfer through the
network. We use this to try to identify unfair behavior of the \gls{RDB}
mechanism.


\section{Main Contributions}\label{sect:introduction-contributions}

The main contributions of this thesis can be summarized as follows:

\begin{enumerate}[leftmargin=1cm,itemindent=0mm,label=\large\textbullet,parsep=0pt]

\item{Implemented a \gls{TCP} \acrfull{CC} module for the \linuxkernel{}, that
    enables a sender host to send redundant data in data packets already
    scheduled for transmission.}

  \item{Evaluated the \glsuseri{thin-stream} classification method in the
      \linuxkernel{} and suggested improvements.}

  \item{Experimentally evaluated the \gls{RDB} implementation with regards to:}
    \begin{enumerate}[itemindent=0mm,label=\large\textbullet,parsep=0pt]
    \item{The tradeoff between latency and aggressiveness in terms of increasing
        the packet size, and hence the throughout.}

    \item{How it gives \gls{RDB} streams significantly better latency without
        being unfair towards competing traffic.}

    \item{How it prevents the \gls{RDB} mechanism from being abused to obtain an
        advantage over competing traffic.}
  \end{enumerate}
\end{enumerate}


\section{Outline}\label{sect:introduction-outline}

The thesis is structured as follows: In \cref{chap:chapter2} we look at the
background for the different interactive applications that produce
\glspl{thin-stream}, and the mechanisms for improving the latency for such
streams in the \linuxkernel{}. We also present \gls{rdb1}, the \gls{TCP}
modification that our work is based on.

In \cref{chap:improving-rdb} we go into detail about what causes the increased
latencies for \glspl{thin-stream}, and aspects of the \gls{rdb1} that
we will try to improve upon. In \cref{chap:rdb-implementation} we present
\gls{rdb2}, the re-implementation of \gls{RDB}, and in
\cref{chap:evaluation} we evaluate the modifications by running experiments and
presenting the results. \Cref{chap:conclusion} concludes the thesis with a
summary of our findings and an outline on topics for future research.
